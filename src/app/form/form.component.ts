import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserServService } from '../user-serv.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  user: User = new User();

  constructor(private userServService:UserServService) { }

  ngOnInit(): void {
  }
  
  onsubmit() {
    this.userServService.enroll(this.user).subscribe((data: any) =>
      console.log("sucess", data),
      (error: any) => console.log('error', error));
      
}


}
