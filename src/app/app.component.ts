import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Stud';
  s: string = '';
  imgi: string =
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH5uRgT4uxfWbC93QLv6SzXaBq1UYqs1mdtg&usqp=CAU';

  tablink: any = document.getElementsByClassName("tab-link");
  tabcontent: any = document.getElementsByClassName("tab-contents")
  
  opentab() {
    return false;
  }
}
