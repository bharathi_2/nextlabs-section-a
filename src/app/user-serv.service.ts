import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { User } from './user';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class UserServService {
  url = 'https:localhost:3000/enroll';

  constructor(private http: HttpClient) {}

  enroll(user: User): Observable<Object> {
    return this.http.post<any>(`${this.url}`, user);
  }
}
